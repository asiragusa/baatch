# Baatch

## Running the tests

### With docker-compose
```bash
docker-compose run --rm test
```

### On the local machine
```bash
bundle install
rspec
```