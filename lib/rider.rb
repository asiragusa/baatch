class Rider
  attr_reader :id, :speed, :route, :delivery_time

  include Coords

  def initialize(id:, speed:, x:, y:)
    @id = id
    @speed = speed
    @x = x
    @y = y

    reset
  end

  # Reset to the initial state
  def reset
    @restaurant = nil
    @delivery_time = 0

    @actual_coords = self

    @route = []
  end

  # Correctly clone the object
  def initialize_copy(orig)
    super
    @route = orig.route.clone
  end

  # True if the rider has no assigned restaurant or if he is going to the same restaurant
  def can_deliver(restaurant:)
    @restaurant == nil || @restaurant.id == restaurant.id
  end

  # Deliver the customer with an order from the restaurant
  def deliver(customer:, restaurant:)
    set_restaurant :restaurant => restaurant

    go_to :coords => customer
  end

  # Compare two riders and find the fastest
  def <(rider)
    return true if rider == nil
    return @delivery_time < rider.delivery_time if @delivery_time != rider.delivery_time
    distance_from_restaurant < rider.distance_from_restaurant
  end

  # Gets the distance from the restaurant
  protected def distance_from_restaurant
    distance :to => @restaurant
  end

  # Computes the time to go to the given coords
  private def time_to_go(coords:)
    (@actual_coords.distance :to => coords) / speed * 60
  end

  # Move the rider to the given coords
  private def go_to(coords:)
    @route << coords
    @delivery_time += time_to_go :coords => coords
    @actual_coords = coords
  end

  # Set the restaurant and initialize distance_from_restaurant and delivery_time
  private def set_restaurant(restaurant:)
    # Do nothing if the restaurant is the same
    return if @restaurant != nil && restaurant.id == @restaurant.id

    # Raise an error if the rider can't deliver the restaurant
    raise 'Impossible to change restaurant' unless can_deliver :restaurant => restaurant

    @restaurant = restaurant

    go_to :coords => restaurant
    @delivery_time = restaurant.cooking_time if @delivery_time < restaurant.cooking_time
  end
end