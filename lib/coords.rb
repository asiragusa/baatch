
module Coords
  attr_reader :x, :y

  # Returns the distance from a point a to a point b
  def distance(to:)
    Math.sqrt((@x - to.x) ** 2 + (@y - to.y) ** 2)
  end
end