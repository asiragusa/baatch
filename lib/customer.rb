require 'coords'

class Customer
  include Coords

  attr_reader :id
  attr_accessor :delivery_time

  def initialize( id:, x:, y:)
    @id = id
    @x = x
    @y = y

    reset
  end

  # Reset to the initial state
  def reset
    @delivery_time = 0
  end
end