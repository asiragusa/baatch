require 'restaurant'
require 'customer'
require 'rider'

class DeliveryRouter
  def initialize(restaurants, customers, riders)
    @restaurants = to_hash :array => restaurants
    @customers = to_hash :array => customers
    @riders = to_hash :array => riders

    reset
  end

  # Converts an array of objects with id to an hash indexed by id
  def to_hash(array:)
    array.map { |item| [item.id, item] }.to_h
  end

  # Add an order
  def add_order(customer:, restaurant:)
    raise "Impossible to add a new order" if @orders[customer] != nil
    raise "Unknown customer id" if @customers[customer] == nil
    raise "Unknown restaurant id" if @restaurants[restaurant] == nil
    @route_changed = true
    @orders[customer] = restaurant
  end

  # Clear the orders for customer
  def clear_orders(customer:)
    raise "Unknown customer id" if @customers[customer] == nil
    raise "Customer has no orders" if @orders[customer] == nil
    @route_changed = true
    @orders.delete customer
  end

  # Resets to the initial state
  def reset
    @orders = {}
    @route_changed = true
  end

  # Get the route for rider
  def route(rider:)
    raise "Unknown rider id" if @riders[rider] == nil
    compute_routes
    @riders[rider].route
  end

  # Get the delivery time for customer
  def delivery_time(customer:)
    raise "Unknown customer id" if @customers[customer] == nil
    raise "Customer has no orders" if @orders[customer] == nil
    compute_routes
    @customers[customer].delivery_time
  end

  # Get the time customers have waited to receive their orders
  def fitness
    compute_routes
    fitness=0
    @orders.each_key { |customer| fitness += @customers[customer].delivery_time}
    fitness
  end

  # Loops across all the orders and finds the best route
  private def compute_routes
    return @routes unless @route_changed

    @route_changed = false

    @riders.each_value { |rider| rider.reset }
    @customers.each_value { |customer| customer.reset }

    @orders.each { |customer_id, restaurant_id|
      compute_best_route :customer => @customers[customer_id], :restaurant => @restaurants[restaurant_id]
    }
  end

  # Loops across all the riders and finds the fastest
  private def compute_best_route(customer:, restaurant:)
    fastest_rider = nil
    @riders.each_key { |k|
      next unless @riders[k].can_deliver :restaurant => restaurant

      # Simulate a delivery and compare the rider to the fastest
      rider = @riders[k].clone
      rider.deliver :customer => customer, :restaurant => restaurant

      fastest_rider = rider if rider < fastest_rider
    }

    @riders[fastest_rider.id].deliver :customer => customer, :restaurant => restaurant
    customer.delivery_time = fastest_rider.delivery_time
  end
end