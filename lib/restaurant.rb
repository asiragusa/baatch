require 'coords'

class Restaurant
  include Coords

  attr_reader :id, :cooking_time

  def initialize( id:, cooking_time:, x:, y: )
    @id = id
    @cooking_time = cooking_time
    @x = x
    @y = y
  end
end