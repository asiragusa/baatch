require 'rider'

describe Rider do
  before(:all) do
    @restaurants = [
        Restaurant.new(:id => 1, :cooking_time => 40, :x => 3, :y => 4),
        Restaurant.new(:id => 2, :cooking_time => 10, :x => 4, :y => 3),
    ]
    @customers = [
        Customer.new(:id => 3, :x => 4, :y => 4),
        Customer.new(:id => 4, :x => 4, :y => 5),
        Customer.new(:id => 5, :x => 5, :y => 5),
    ]
  end

  describe "#can_deliver" do
    before(:all) do
      @rider = Rider.new(:id => 1, :speed => 10, :x => 0, :y => 0)
    end
    context "given rider is delivering from restaurant 1" do
      before(:all) do
        @rider.deliver :customer => @customers[0], :restaurant => @restaurants[0]
      end
      it "can deliver another customer from restaurant 1" do
        expect(@rider.can_deliver :restaurant => @restaurants[0]).to be true
      end
      it "can't deliver another customer from restaurant 2" do
        expect(@rider.can_deliver :restaurant => @restaurants[1]).to be false
      end
    end
  end

  describe "#deliver" do
    before(:all) do
      @rider = Rider.new(:id => 1, :speed => 10, :x => 0, :y => 0)
    end
    context "given rider is delivering customer 1 from restaurant 1" do
      before(:all) do
        @rider.deliver :customer => @customers[0], :restaurant => @restaurants[0]
      end

      it "can deliver customer 1 in 46 minutes" do
        expect(@rider.delivery_time).to eql(46.0)
      end

      it "can deliver customer 2 in 52 minutes" do
        @rider.deliver :customer => @customers[1], :restaurant => @restaurants[0]
        expect(@rider.delivery_time).to eql(52.0)
      end

      it "must raise an exception if customer 2 orders from restaurant 2" do
        expect {
          @rider.deliver :customer => @customers[1], :restaurant => @restaurants[1]
        }.to raise_exception("Impossible to change restaurant")
      end
    end
  end

  describe "#<" do
    before(:each) do
      @rider1 = Rider.new(:id => 1, :speed => 10, :x=>0, :y => 0)
      @rider2 = Rider.new(:id => 2, :speed => 10, :x=>1, :y => 1)
    end
    context "given rider 1 and rider 2" do
      it "rider 1 < nil for customer 1" do
        @rider1.deliver :customer => @customers[0], :restaurant => @restaurants[0]
        expect(@rider2 < nil).to be true
      end

      it "rider 2 < rider 1 for customer 2 because delivery_time" do
        @rider1.deliver :customer => @customers[0], :restaurant => @restaurants[0]
        @rider1.deliver :customer => @customers[1], :restaurant => @restaurants[0]
        @rider2.deliver :customer => @customers[1], :restaurant => @restaurants[0]
        expect(@rider2 < @rider1).to be true
      end

      it "rider 2 < rider 1 for customer 1 because distance from restaurant" do
        @rider1.deliver :customer => @customers[0], :restaurant => @restaurants[0]
        @rider2.deliver :customer => @customers[0], :restaurant => @restaurants[0]
        expect(@rider2 < @rider1).to be true
      end
    end
  end
end
