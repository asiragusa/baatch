require 'coords'

describe Coords do
  class Coord
    include Coords

    def initialize(x:, y:)
      @x = x
      @y = y
    end
  end

  describe "setters and getters" do
    context "given a Coord (1,2)" do
      before(:all) do
        @coord = Coord.new(:x => 1, :y => 2)
      end

      it "returns 1 for coord.x" do
        expect(@coord.x).to eql(1)
      end
      it "returns 2 for coord.y" do
        expect(@coord.y).to eql(2)
      end
    end
  end

  describe "#distance" do
    context "given two Coords (0,0) and (3,4)" do
      it "returns a distance of 5.0" do
        point_a = Coord.new(:x => 0, :y => 0)
        point_b = Coord.new(:x => 3, :y => 4)

        expect(point_a.distance(:to => point_b)).to eql(5.0)
      end
    end
  end
end


